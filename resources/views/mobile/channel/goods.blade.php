@extends('mobile.layouts.layout')
@section('content')
    <div class="goods-list-h">
        @component('mobile.component.sort_bar',['url'=>'channel/'.$id,'desc'=>$desc,'sort'=>$sort])
        @endcomponent
        @if(count($list) > 0)
            <ul id="result">
                @foreach($list as $item)
                    @component('mobile.component.goods_list_item_h',['url'=>'/item','item'=>$item])
                    @endcomponent
                @endforeach
            </ul>
        @else
            @include('mobile.component.empty')
        @endif
    </div>
@endsection
@section('script')
    <script>
        $(function () {
            nextPageUrl                     =   '{{url()->current()}}';
            $('a[href="'+nextPageUrl+'"]').closest('li').addClass('active');
            var page                        =   2;
            $(window).scroll(function () {
                var scrollTop = $(this).scrollTop();
                var scrollHeight = $(document).height();
                var windowHeight = $(this).height();
                if (scrollTop + windowHeight == scrollHeight) {
                    nextPage(nextPageUrl+'?page='+page,'#result');
                    page++;
                }
            });
        })

    </script>
@endsection
